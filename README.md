# ccache-gradlerio

batch (.bat, Windows) and shell (.sh, Linux) scripts to ease use of ccache with gradlerio.

## Why this is needed

Gradle does a bad job of figuring out cpp header dependencies under some circumstances.
If you trigger those circumstances, incremental building gets essentially turned off and a full 
recompilation takes place every time `gradlew build` is done. This can take several minutes even
though only a few files might really need to be recompiled.

## What's here
* `ccache-gradlerio-helper.sh` is a shell script that invokes ccache to then invoke the compiler. The way it works is you rename the real compiler and symlink ccache-gradlereio-helper.sh as the filename of the compiler that gradle will use (More details below). The script can be used this way for multiple different compilers.
* `ccache-gradlerio-helper.bat` is a batch file that invokes `ccache` which in turn invokes the real compiler.  Unfortunately on Windows things aren't as easy as on linux -- you actually have to make a .exe out of the batch file before it will work for our purposes. The tool I used for this is: `Advanced Bat to Exe Converter v4.11`. Also
unfortunate is that the .exe it produces doesn't really work for our purposes -- it seems to make a temp
batch file that gets corrupted when multiple instances run concurrently. So this is not usable.
Also unfortunate is that the online tool `bat 2 exe` does not properly handle the argument list in this batch file so you have to  download the batch file converter and do it yourself. I could not find any Bat to Exe converter that worked.

* `ccache-gradlerio-helper.py` -- does the same thing as the .bat; it can be successfully compiled to a .exe using pyinstaller. See the pyinstaller online docs. Make sure to use the -F switch so that the .exe is self-contained. The only unfortunate aspect of this approach is the almost 7MB size of the resulting .exe.

* `install.sh` for Linux; renames the g++ compiler found in the `~/wpilib/2020/roborio/bin`, copies ccache-gradlerio-helper.sh to that directory, and creates a symlink to the shell file from the old name of the compiler. Checks to make sure that ccache is installed, and that the compiler is where it expects it to be.

* `install.bat` for Windows; renames the g++ compiler found in `c:\users\Public\wpilib\2020\roborio\bin`, copies ccache-gradlerio-helper.exe and ccache.exe to that directory, and creates a copy of ccache-gradlerio-helper.exe with the old name of the compiler. (A symlink does not seem to work -- at least with the .bat file arg0 was the name
of the symlink target instead of the symlink.)

## Installation
1. Make sure you've installed the frc toolchains following frc's instructions
2. On linux make sure that you've installed the ccache package from your distribution and that ccache is installed in `/usr/bin`

Download the ccache-gradlerio-2020.zip from the Downloads folder. Expand it. Notice that it contains ccache.exe and ccache-gradlerio-helper.exe in addition to the .bat and .sh scripts mentioned above

Invoke either `sh install.sh` or `install.bat` depending on whether you are using Linux or Windows. On Linux
the default installation location is `~/wpilib/2020/roborio/bin` and on Windows the default location is
`C:\Users\Public\wpilib\2020\roborio\bin`. Depending on how you installed it the toolchain may be in any of
several other places. The easiest way to figure out where is to run `./gradlew --info build` in the VSCode
terminal and find out the path to the `g++` compiler that is being used. You can pass the path to the
`install` scripts: `sh install.sh path` or `install.bat path`.

## Uninstall
Use either `uninstall.sh` or `uninstall.bat` as appropriate. Like the installation scripts, these take an
optional installation location as an argument.

If you receive any errors please let me know: carl.hauser@gmail.com. In particular, I suspect that on Windows if you've installed the toolchain for only yourself rather than "All Users" it may not work. I would like to get it working but I don't have such an installation to play with. So please get in touch and we'll figure it out.

## Uninstall
* `sh uninstall.sh` or `uninstall.bat` depending on whether you are using Linux or Windows.

## If the toolchain gets updated by frc
Uninstall before updating the toolchain, then reinstall after.

## Building the .exe files
The zip file contains ccache.exe and ccache-gradlerio-helper.exe. I built ccache.exe by downloading the sources for ccache in a mingw environment in which I had installed the compiler and build tools. Note that `make` is not named `make` but something like `mingw32-make` in this environment.

I tried various tools to create a .exe from a .bat but nothing worked fully correctly. The current
`ccache-gradlerio-helper.exe` was created from ccache-gradlerio-helper.py using `pyinstaller`. The .exe is huge but it works.