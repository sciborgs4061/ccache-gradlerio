@echo off
set tooldir=C:\Users\Public\wpilib\2020\roborio\bin\
if "$1" geq 1 (
    set tooldir=$1
)

echo "Uninstalling from " %tooldir%
if not exist %tooldir%real-arm-frc2020-linux-gnueabi-g++.exe (
   echo "ccache-roborio-helper does not appear to be installed"
   exit /b
)
pushd %tooldir%
del ccache-gradlerio-helper.exe 
del ccache.exe 
del "arm-frc2020-linux-gnueabi-g++.exe"
ren real-arm-frc2020-linux-gnueabi-g++.exe arm-frc2020-linux-gnueabi-g++.exe
popd
