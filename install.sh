# use this by
#   sh install.sh
#
if [ $# -eq 0 ]
then
    loc=~/wpilib/2020/roborio/bin/
else
    loc=$1
fi
echo "installing ccache-gradlerio-helper to ${loc}"

mv -n ${loc}arm-frc2020-linux-gnueabi-g++ ${loc}real-arm-frc2020-linux-gnueabi-g++
if [ $? -ne 0 ]
then
    echo "Moving the real compiler didn't work."
    echo "Is ccache-gradlerio-helper already installed? Is the gradlerio toolchain installed in the expected location?"
    echo "Inspect this installation script and follow its steps manually to figure out the exact problem"
    exit 1
fi
cp ccache-gradlerio-helper.sh ${loc}
if [ $? -ne 0 ]
then
    echo "Installing the helper .sh file didn't work"
    echo "Is the gradlerio toolchain installed in the expected location?"
    echo "Inspect this installation script and follow its steps manually to figure out the exact problem"
    exit 1
fi
( cd ${loc}; ln -s ccache-gradlerio-helper.sh arm-frc2020-linux-gnueabi-g++; )
if [ $? -ne 0 ]
then
    echo "Making the symlink to the helper script didn't work"
    echo "Is ccache-gradlerio-helper already installed? Is the gradlerio toolchain installed in the expected location?"
    echo "Inspect this installation script and follow its steps manually to figure out the exact problem"
    exit 1
fi
( cd ${loc};  chmod 755 ccache-gradlerio-helper.sh )
if [ $? -ne 0 ]
then
    echo "Making the helper script executable didn't work"
    echo "Inspect this installation script and investigate why."
    exit 1
fi
