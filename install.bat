@echo off
set tooldir=C:\Users\Public\wpilib\2020\roborio\bin\
if "$1" geq 1 (
    set tooldir=$1
)
echo "Installing to " %tooldir%
if exist %tooldir%real-arm-frc2020-linux-gnueabi-g++.exe (
   echo "ccache-roborio-helper appears to be installed already"
   exit /b
)
copy ccache-gradlerio-helper.exe /b %tooldir%
copy ccache.exe /b %tooldir%
ren %tooldir%arm-frc2020-linux-gnueabi-g++.exe real-arm-frc2020-linux-gnueabi-g++.exe
rem symlinking to a common copy of the helper.exe doesn't work
rem mklink %tooldir%arm-frc2020-linux-gnueabi-g++.exe %toodir%ccache-gradlerio-helper.exe
rem instead make a copy of the file
pushd %tooldir%
copy ccache-gradlerio-helper.exe /b "arm-frc2020-linux-gnueabi-g++.exe"
popd
