from sys import argv
from os import path, system

(cmdPath,cmd) = path.split(argv[0])
ccache = path.join(cmdPath, "ccache.exe")
realcmd = path.join(cmdPath, "real-"+cmd)
cmdline = " ".join([ccache, realcmd, " ".join(argv[1:])])
system(cmdline)