# use this by
#   sh uninstall.sh
#
if [ $# -eq 0 ]
then
    loc=~/wpilib/2020/roborio/bin/    
else
    loc=$1
fi
echo "uninstalling ccache-gradlerio-helper from ${loc}"
rm ${loc}ccache-gradlerio-helper.sh
if [ $? -ne 0 ]
then
    echo "Removing the helper script did not work."
    echo "Is ccache-gradlerio-helper installed? Is the gradlerio toolchain installed in the expected location?"
    echo "Inspect this installation script and follow its steps manually to figure out the exact problem"
    exit 1
fi
mv ${loc}real-arm-frc2020-linux-gnueabi-g++ ${loc}arm-frc2020-linux-gnueabi-g++
if [ $? -ne 0 ]
then
    echo "Putting the real compiler back in place did not work."
    echo "Is ccache-gradlerio-helper installed? Is the gradlerio toolchain installed in the expected location?"
    echo "Inspect this installation script and follow its steps manually to figure out the exact problem"
    exit 1
fi
